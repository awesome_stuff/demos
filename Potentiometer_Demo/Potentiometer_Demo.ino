
int pot = A5;

void setup() {                
  pinMode(pot, INPUT);   
  Serial.begin(9700);  
}

// the loop routine runs over and over again forever:
void loop() {
  int potReading = analogRead(pot);
  int potDegC = analogRead(pot) / 12.7875;
  int potDegF = (potDegC * (9.0 / 5.0)) + 32;
  Serial.print("Celsius: "); 
  Serial.print(potDegC);
  Serial.print("\n");
  Serial.print("Fahrenheight: "); 
  Serial.print(potDegF);
  Serial.print("\n");
  delay(10);
}
