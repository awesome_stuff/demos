#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(7, 8, 5, 4, 3, 2);
int contrast = A3;
int pot = A5;
int sensor_neck = A6;
int sensor_abdomen = A7;
int oldPotValue = 0;
int oldNeckValue = 0;
int oldAbdomenValue = 0;
int backlight = 8;
boolean setScreen = false;
void setup() {
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  pinMode(contrast, OUTPUT);
  pinMode(pot, INPUT);
  pinMode(sensor_neck, INPUT);
  pinMode(sensor_abdomen, INPUT);
  analogWrite(contrast, 0);
  Serial.begin(9600);
}

void loop() {
  int newPotValue = analogRead(pot);
  int newNeckValue = analogRead(sensor_neck);
  int newAbdomenValue = analogRead(sensor_abdomen);
  int setPotCels = newPotValue / 10.23;
  int setPotFahr = (setPotCels * (9/5)) + 32;
  double neckVoltage = newNeckValue * 0.004882814;
  double abdomenVoltage = newAbdomenValue * 0.004882814;
  double neckCels = (neckVoltage - 0.5) * 100.0;
  double abdomenCels = (abdomenVoltage - 0.5) * 100.0;
  double neckFahr = neckCels * (9.0/5.0) + 32.0;
  double abdomenFahr = abdomenCels * (9.0/5.0) + 32.0;
  
  if ((newPotValue - oldPotValue) >= 5 || (oldPotValue - newPotValue) >= 5) {
    setScreen = true;
    lcd.setCursor(0, 0);
    lcd.clear();    
    lcd.print("Target Temp:");
    lcd.setCursor(0, 1);
    lcd.print(setPotCels);
    lcd.print("C | ");
    lcd.print(setPotFahr);
    lcd.print("F");
    Serial.print("\nTarget Temp:\n");
    Serial.print(setPotCels);
    Serial.print("C | ");
    Serial.print(setPotFahr);
    Serial.print("F");
  } else {
    if (setScreen) {
      setScreen = false;
      delay(750);
    }    
    lcd.setCursor(0, 0);
    lcd.clear();
    lcd.print("Body Temp:");
    lcd.setCursor(0, 1);
    lcd.print(neckCels);
    lcd.print("C | ");
    lcd.print(neckFahr);
    lcd.print("F");
    Serial.print("\nBody Temp:\n");
    Serial.print(neckCels);
    Serial.print("C | ");
    Serial.print(neckFahr);
    Serial.print("F");
  }
  oldPotValue = newPotValue;
  delay(1000);
}

